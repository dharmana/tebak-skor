<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
            parent::__construct();
            $this->load->model('m_base');

    }

	public function index()
	{
		$this->load->view('home');
	}

  public function submit_answer()
	{
    $data = [
			'user' => $_POST['user'],
			'skor1' => $_POST['skor1'],
			'skor2' => $_POST['skor2']
		];
		$save = $this->db->insert('records', $data);
    redirect('home/finish');
	}
  public function finish()
	{
		$this->load->view('finish');
	}
}
