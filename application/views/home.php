<html>
<head>

<title>TEBAK SKOR</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

</head>
<body>

<div class="panel" style="width:500px; margin:0 auto;">
<center>
  <br>
  <h1 style="margin:0; padding:0">TEBAK SKOR</h1>
  <h3>EURO 2016</h3><br>
</center>

<form class="form-horizontal" method="POST" action="<?= base_url('index.php/home/submit_answer'); ?>">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">User ID</label>

                  <div class="col-sm-10">
                    <input required name="user" type="text" class="form-control" placeholder="Masukkan User ID anda">
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">France</label>
                  <div class="col-sm-10">
                    <select name="skor1" class="form-control">
                      <?php for($i=0; $i<=7; $i++) { ?>
                        <option><?= $i ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label  class="col-sm-2 control-label">Portugal</label>
                  <div class="col-sm-10">
                    <select name="skor2" class="form-control">
                      <?php for($i=0; $i<=7; $i++) { ?>
                        <option><?= $i ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a class="btn btn-default">BATAL</a>
                <button type="submit" class="btn btn-info pull-right">KIRIM</button>
              </div>
              <!-- /.box-footer -->
            </form>
</div>
</body>
</html>
