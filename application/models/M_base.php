<?php
class m_base extends CI_Model {

    public $title;
    public $content;
    public $date;

    var $login_page = 'login';

    function __construct() {
        parent::__construct();
    }

    /* --------------------------------------------------------------
     * AUTH_USER
     * Function for check logged in user. If logged in is false, user
     * will be redirected to login page;
     * 
     * Doc :
     * - $session       : session user data for authentication
     * ----------------------------------------------------------- */

    function auth_user($session) {
        if(!$session) {
                redirect($login_page);
        }
    }

    /* --------------------------------------------------------------
     * CUSTOM_QUERY
     * Function for run query with custom query that user want
     * 
     * Doc :
     * - $query       : SQL query. ex; 'select * from users'
     * ----------------------------------------------------------- */

    function custom_query($query){
        $query = $this->db->query($query);
        return $query->result_array();
    }

    /* --------------------------------------------------------------
     * GET ROW
     * Function for show single row result
     * 
     * Doc :
     * - $table_name    : table name from database. ex: 'users'
     * - $query_select : columns that showed ex: '*' or 'name, email'
     * ----------------------------------------------------------- */

    function get_row($table_name, $query_select, $condition = array(), $join = array(), $type = '', $order_by = '', $order_type = 'ASC', $limit = '',$limit_start = '', $condition_or_where_in = array()) {
        $this->db->from($table_name);
        foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
        }
    
        return $this->db->get()->row();
    }

      function num_data( $table_name, $query_select, $condition = array(), $join = array(), $type = '', $order_by = '', $order_type = 'ASC', $groupby = '', $limit = '',$limit_start = '', $condition_or_where_in = array() ) {
        $this->db->select($query_select, FALSE);
        //$this->db->where($table_name . '.status', 'A');
        foreach ($condition as $key => $value) {
          if($value!="") {
            $this->db->where($key, $value);
          } else {
            $this->db->where($key);
          }

        }

        foreach ($condition_or_where_in as $key => $value) {
            $this->db->or_where_in($key, $value);
        }

        if ($groupby != '') {
                    $this->db->group_by($groupby);
        }



        foreach ($join as $key => $value) {
            $this->db->join($key, $value, $type);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order_type);
        }

        if ($limit != '' && $limit_start !='') {
            $this->db->limit($limit,$limit_start);
        }
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->num_rows();

        }

    function validate($level, $username, $password) {
           $this ->db->select('username, password');
           $this->db->from('tb_'.$level);
           $this->db->where('username', $username);
           $this->db->where('password', MD5($password));
           $this->db->limit(1);
         
           $query = $this-> db->get();
         
           if($query->num_rows() == 1)
           {
             return $query->result();
           }
           else
           {
             return false;
           }
    }

    function update($table, $data, $condition = array()) {
        foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
        }
        
        if($this->db->update($table, $data)) {
            return TRUE;
        }
    }

    function get_list($table_name, $query_select, $condition = array(), $join = array(), $type = '', $order_by = '', $order_type = 'ASC', $groupby = '', $limit = '',$limit_start = '', $condition_or_where_in = array()) {
        $this->db->select($query_select, FALSE);
        //$this->db->where($table_name . '.status', 'A');
        foreach ($condition as $key => $value) {
          if($value!="") {
            $this->db->where($key, $value);
          } else {
            $this->db->where($key);
          }

        }

        foreach ($condition_or_where_in as $key => $value) {
            $this->db->or_where_in($key, $value);
        }

        if ($groupby != '') {
                    $this->db->group_by($groupby);
        }



        foreach ($join as $key => $value) {
            $this->db->join($key, $value, $type);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order_type);
        }

        if ($limit != '' && $limit_start !='') {
            $this->db->limit($limit,$limit_start);
        }

        $query = $this->db->get($table_name);
//echo $this->db->queries[0];
        return $query->result_array();
    }

    function is_duplicate_row($table, $field_name, $value, $id = '') {
        if ($id == '') {
            $this->db->select($field_name);
            $this->db->where('UPPER(' . $field_name . ')', strtoupper($value));
            $this->db->where('status', 'A');
            $query = $this->db->get($table);
            $num = $query->num_rows();
            if ($num > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $this->db->select($field_name);
        
        
            $this->db->where('UPPER(' . $field_name . ')', strtoupper($value));
            $query = $this->db->get($table);
            $num = $query->num_rows();
            if ($num > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    function delete_row($table_name = '', $field_id = '', $id) {
        $this->db->where('md5(' . $field_id . ')', $id);
        $data['status'] = 'D';
        $this->db->update($table_name, $data);
    }

    function delete_permanent_row($table_name = '', $field_id = '', $id) {
        $this->db->where('md5(' . $field_id . ')', $id);
        $this->db->delete($table_name);
        if ($this->db->_error_message()) {
            return 0;
        } else {
            return 1;
        }
    }

    function get_list_for_select($table = '', $condition = array()) {

        $data = array();
        $this->db->where('status', 'A');
        foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
        }
        $Q = $this->db->get($table);
        if ($Q->num_rows() > 0) {
            foreach ($Q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $Q->free_result();
        return $data;
    }

    function get_row_join($table_name, $query_select, $condition = array(), $join = array()) {
        $this->db->select($query_select, FALSE);
        foreach ($condition as $key => $value) {
            $this->db->where($key, $value);
        }
        foreach ($join as $key => $value) {
            $this->db->join($key, $value, '');
        }
        $res = $this->db->get($table_name);
        return $res->row();
    }

    function save_with_get_id($table, $data) {
        $this->db->trans_start();
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function lookup($table_name, $keyword) {
        $query = $this->db->query("SELECT *
                        FROM " . $table_name . "
                        where name like '" . $keyword . "%'");
        return $query->result();
    }

    function save($table, $data) {
      
      
          
            $this->db->insert($table, $data);
      
        return TRUE;
    }

    function get_menu_list(){
        $query = $this->db->query("SELECT *
                                    FROM  menu
                                    ");
            return $query->result_array();
    }
        
        
            function get_property_list(){
        $query = $this->db->query("SELECT *
                                            FROM  property
                                            where status='A'
                                            ");
            return $query->result_array();
    }

    function get_role($role_id){
        $query = $this->db->query("SELECT  menu.* 
                                    FROM role
                                    JOIN role_list ON role.id = role_list.role_id
                                    JOIN menu ON role_list.menu_id = menu.id
                                    WHERE role.id =".$role_id);
            return $query->result_array();
    }
        
        

    function get_role_list($admin_id){
        $query = $this->db->query("SELECT  menu.id,menu.name 
                                    FROM  admin 
                                    JOIN role ON admin.role_id = role.id
                                    JOIN role_list ON role.id = role_list.role_id
                                    JOIN menu ON role_list.menu_id = menu.id
                                    WHERE admin.id = ".$admin_id);
            return $query->result_array();
    }


    function get_room_facilities($room_id) {
        $query = $this->db->query("SELECT  room_facilities.* 
                                              FROM room_facilities
                                            WHERE room_id=" . $room_id . "
                                           
                                          ");
        return $query->result_array();
    }

    function update_password($id, $newpassword) {
        $this->db->where('id', $id);
        $data['password'] = $newpassword;
        $this->db->update('admin', $data);
    }

    function update_password_property($id, $newpassword) {
        $this->db->where('id', $id);
        $data['password'] = $newpassword;
        $this->db->update('property', $data);
    }

       function auto_number($tabel, $kolom, $lebar=0, $awalan) {
        $this->db->select($kolom);
        $this->db->order_by($kolom, "desc");
        $this->db->limit(1);
        $this->db->from($tabel);
        $query = $this->db->get();
        $rslt = $query->result_array();

        $total_rec = $query->num_rows();
        if ($total_rec == 0) {
        $nomor = 1;
        } else {
        $nomor = intval(substr($rslt[0][$kolom],strlen($awalan))) + 1;
        }

        if($lebar > 0) {

            $angka = $awalan.str_pad($nomor,$lebar,"0",STR_PAD_LEFT);

        } else {

            $angka = $awalan.$nomor;
        }
        return $angka;

    }

    function auto_number_date($tabel, $kolom, $lebar=0, $awalan) {
        $this->db->select($kolom);
        $this->db->order_by($kolom, "desc");
        $this->db->limit(1);
        $this->db->from($tabel);
        $query = $this->db->get();
        $rslt = $query->result_array();
        $awalan = $awalan . date("ym");
        $total_rec = $query->num_rows();
        if ($total_rec == 0) {
        $nomor = 1;
        } else {
        $nomor = intval(substr($rslt[0][$kolom],strlen($awalan))) + 1;
        }

        

        if($lebar > 0) {
            $angka = $awalan.str_pad($nomor,$lebar,"0",STR_PAD_LEFT);
        } else {
            $angka = $awalan.$nomor;
        }
        return $angka;

    }

     /* --------------------------------------------------------------
     * RESIZE CROP IMAGE
     * Function for resize and crop image
     * 
     * Doc :
     * - $i_width    : width of cropped images
     * - $i_height   : height of cropped images
     * ----------------------------------------------------------- */

function resize_crop_image($i_width=300, $i_height=300){

$config['upload_path'] = 'uploads';
      $config['encrypt_name'] = TRUE;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
            $config['max_size'] = '5000';
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('file')) {
                echo $this->upload->display_errors();
            } else {

      $file_data = $this->upload->data();

      $upload_data = $this->upload->data();
      $image_config["image_library"] = "gd2";
      $image_config["source_image"] = $upload_data["full_path"];
      $image_config['create_thumb'] = FALSE;
      $image_config['maintain_ratio'] = TRUE;
      $image_config['new_image'] = $upload_data["file_path"] . '_tmp.png';
      $image_config['quality'] = "100";


      $image_config['width'] = $i_width;
      $image_config['height'] = $i_height;
      $dim = (intval($upload_data["image_width"]) / intval($upload_data["image_height"])) - ($image_config['width'] / $image_config['height']);
      $image_config['master_dim'] = ($dim > 0)? "height" : "width";

      $this->load->library('image_lib');
      $this->image_lib->initialize($image_config);

      if(!$this->image_lib->resize()){ //Resize image
        redirect("errorhandler"); //If error, redirect to an error page
      }

      $image_config['image_library'] = 'gd2';
      $image_config['source_image'] = $upload_data["file_path"] . '_tmp.png';
      $image_config['new_image'] = $upload_data["file_path"] . $file_data['file_name'];
      $image_config['quality'] = "100%";
      $image_config['maintain_ratio'] = FALSE;

      $vals = @getimagesize($upload_data["file_path"] . '_tmp.png');
        $width = $vals['0'];
        $height = $vals['1'];
        $image_config['x_axis'] = ($width - $i_width)/2;
        $image_config['y_axis'] = ($height- $i_height)/2;


      $this->image_lib->clear();
      $this->image_lib->initialize($image_config);

      if (!$this->image_lib->crop()){
            redirect("errorhandler"); //If error, redirect to an error page
      }
             return $file_data['file_name'];
      }

}





}