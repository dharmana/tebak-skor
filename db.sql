-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 09 Jul 2016 pada 12.45
-- Versi Server: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tebakskor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `records`
--

CREATE TABLE `records` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `skor1` int(11) NOT NULL,
  `skor2` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `records`
--

INSERT INTO `records` (`id`, `user`, `skor1`, `skor2`, `created_at`) VALUES
(1, 'wayan', 0, 0, '2016-07-09 10:43:40'),
(2, 'wayanw', 0, 0, '2016-07-09 10:44:07'),
(3, 'wayanwww', 5, 0, '2016-07-09 10:44:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
